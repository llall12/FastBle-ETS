# FastBle-ETS

## 简介

FastBle-ETS是一个处理蓝牙BLE设备的库，可以对蓝牙BLE设备进行过滤，扫描，连接，读取，写入等。

![gif](preview/operation.gif)

## 下载安装
```shell
npm install @ohos/wxFastBle --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明
1. 初始化
```
BleManager.getInstance().init();
```
2. 初始化配置
```
BleManager.getInstance()
            .enableLog(true)
            .setReConnectCount(1, 5000)
            .setConnectOverTime(20000)
            .setOperateTimeout(5000);
```
3. 配置扫描规则
```
let scanRuleConfig: BleScanRuleConfig = new BleScanRuleConfig.Builder()
              .setServiceUuids(serviceUuids)      // 只扫描指定的服务的设备，可选
              .setDeviceName(true, names)         // 只扫描指定广播名的设备，可选
              .setDeviceMac(mac)                  // 只扫描指定mac的设备，可选
              .setAutoConnect(isAutoConnect)      // 连接时的autoConnect参数，可选，默认false
              .setScanTimeOut(10000)              // 扫描超时时间，可选，默认10秒
              .build();
BleManager.getInstance().initScanRule(scanRuleConfig);
```
4. 扫描设备
```
BleManager.getInstance().scan(new class extends BleScanCallback {
    @Override
    onScanStarted(success: boolean): void {
        console.log("onScanStarted");
        _this.clearScanDevice();
        _this.is_loading = true;
        _this.loading_rotate = 360;
        _this.btn_scan_text = $r('app.string.stop_scan');
        _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();
    }
    
    @Override
    onLeScan(bleDevice: BleDevice): void {
        console.log("onLeScan");
    }
    
    @Override
    onScanning(bleDevice: BleDevice): void {
        console.log("onScanning");
        ArrayHelper.add(_this.bleDeviceList, bleDevice);
    }
    
    @Override
    onScanFinished(scanResultList: Array<BleDevice>): void {
        console.log("onScanFinished");
        _this.is_loading = false;
        _this.loading_rotate = 0;
        _this.btn_scan_text = $r('app.string.start_scan');
        _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();
    }
})
```
5. 连接设备
```
BleManager.getInstance().connect(bleDevice, new class extends BleGattCallback {
    public onStartConnect(): void {
        _this.progressDialogCtrl.open();
    }
    
    public onConnectFail(bleDevice: BleDevice, exception: BleException): void {
        _this.progressDialogCtrl.close();
    }
    
    public onConnectSuccess(bleDevice: BleDevice, gatt: /*bluetooth.GattClientDevice*/any, status: number): void {
        _this.progressDialogCtrl.close();
    }
    
    public onDisConnected(isActiveDisConnected: boolean, device: BleDevice, gatt: /*bluetooth.GattClientDevice*/any, status: number): void {
        _this.progressDialogCtrl.close();
    }
});
```
6. 取消扫描
```
BleManager.getInstance().cancelScan();
```
7. 读数据
```
BleManager.getInstance().read(
    this.device,
    this.characteristic.serviceUuid,
    this.characteristic.characteristicUuid,
    new class extends BleReadCallback {
    
        public onReadSuccess(data: Uint8Array): void {
            setTimeout(()=> {
                _this.appendDataText(HexUtil.formatHexString(data, true));
            });
        }
    
        public onReadFailure(exception: BleException): void {
            setTimeout(()=> {
                _this.appendDataText(exception.toString());
            });
        }
    });
```
8. 写数据
```
BleManager.getInstance().write(
    this.device,
    this.characteristic.serviceUuid,
    this.characteristic.characteristicUuid,
    HexUtil.hexStringToBytes(hex),
    true,
    true,
    0,
    new class extends BleWriteCallback {
    
        public onWriteSuccess(current: number, total: number, justWrite: Uint8Array): void {
            setTimeout(()=> {
                _this.appendDataText("write success, current: " + current
                        + " total: " + total
                        + " justWrite: " + HexUtil.formatHexString(justWrite, true));
            });
        }
    
        public onWriteFailure(exception: BleException): void {
            setTimeout(()=> {
                _this.appendDataText(exception.toString());
            });
        }
    });
```

## 接口说明
1. 获取实例
   `public static getInstance(): BleManager;`
2. 配置扫描规则
   `public initScanRule(config: BleScanRuleConfig)`
3. 扫描设备
   `public scan(callback: BleScanCallback)`
4. 取消扫描
   `public cancelScan()`
5. 连接设备
   `public connect(device: BleDevice | string, bleGattCallback: BleGattCallback)`
6. 判断设备是否连接
   `public isConnected(device: BleDevice | string): boolean`
7. 断开连接
   `public disconnect(bleDevice: BleDevice)`
8. 读数据
   `public read(bleDevice: BleDevice, uuid_service: string, uuid_read: string, callback: BleReadCallback)`
9. 写数据
   `public write(bleDevice: BleDevice, uuid_service: string, uuid_write: string, data: Uint8Array, split: boolean=true, sendNextWhenLastSuccess: boolean=true, intervalBetweenTwoPackage: number=0, callback: BleWriteCallback)`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- FastBle-ETS
|     |---- entry  # 示例代码文件夹
|     |---- wxFastBle  # fastble库文件夹
|	    |----src
          |----main
              |----ets
                  |----bluetooth 核心逻辑
                  |----callback #回调处理
                  |----exception #异常事件处理
                  |----scan #蓝牙扫描实现
                  |----data #数据处理实现
                  |----utils #工具类
                  |----BleManager.ets #蓝牙连接管理
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/FastBle-ETS/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/FastBle-ETS/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/FastBle-ETS/blob/master/LICENSE) ，请自由地享受和参与开源。
